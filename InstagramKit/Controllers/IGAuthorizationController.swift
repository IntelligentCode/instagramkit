//
//  AuthorizationController.swift
//  InstagramKit
//
//  Created by Артём Шляхтин on 07.09.16.
//  Copyright © 2016 Артём Шляхтин. All rights reserved.
//

import UIKit
import Network

@objc
public protocol InstagramAuthorizationDelegate: NSObjectProtocol {
    func authorizationSuccessfull(_ token: String, userId: String)
    @objc optional func authorizationFailure(_ error: NSError)
}

open class IGAuthorizationController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    open weak var delegate: InstagramAuthorizationDelegate?
    
    
    // MARK: - Lifecycle
    
    open override func loadView() {
        super.loadView()
    }

    open override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        webView.isHidden = true
        clearCookies()
        
        let request = URLRequest(url: URL.Authorization())
        webView.loadRequest(request)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }

    open override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // MARK: - Actions
    
    func showAlert(_ message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func close(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - Private
    
    fileprivate func clearCookies() {
        URLCache.shared.removeAllCachedResponses()
        if let cookies = HTTPCookieStorage.shared.cookies {
            cookies.forEach({ (cookie) in
                HTTPCookieStorage.shared.deleteCookie(cookie)
            })
        }
    }

}

extension IGAuthorizationController: UIWebViewDelegate {
    
    public func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let redirect = URLComponents(string: Instagram.RedirectURI)!
        let component = URLComponents(url: request.url!, resolvingAgainstBaseURL: false)!
        if component.host == redirect.host {
            if let code = (component.queryItems?.filter { $0.name == "code" })?.first?.value {
                let request = Instagram.requestAccessToken(code)
                var packet = NetworkPacket(url: request.URL, body: request.Params.data(using: .utf8))
                packet.method = .POST
                let sendOperation = NetworkOperation.request(packet).response(responseNetworkOperation)
                sendOperation.start()
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                return false
            }
        }
        return true
    }
    
    public func webViewDidFinishLoad(_ webView: UIWebView) {
        webView.isHidden = false
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    public func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print(#function)
    }
    
}


// MARK: - Network Delegation

extension IGAuthorizationController {
    
    public func responseNetworkOperation(result: NetworkOperationResult) {
        switch result.success {
             case true: responseOperationSuccess(data: result.data)
            case false: responseOperationFailure(error: result.error)
        }
    }
    
    func responseOperationSuccess(data: Data?) {
        guard let answer = data else {
            showAlert("Пустой ответ от Instagram")
            return
        }
        parse(answer)
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        self.dismiss(animated: true, completion: nil)
    }
    
    func responseOperationFailure(error: Error?) {
        guard let exception = error else {
            showAlert("Неизвестная ошибка")
            return
        }
        showAlert(exception.localizedDescription)
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    fileprivate func parse(_ data: Data) {
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject]
            guard let token = json?["access_token"] as? String, let userId = json?["user"]?["id"] as? String else {
                showAlert("Ошибка: в ответе отсутсвуют необходимые поля")
                return
            }
            
            delegate?.authorizationSuccessfull(token, userId: userId)
        } catch {
            showAlert("Ошибка: парсер")
        }
    }
    
}
