//
//  NSURL+Additional.swift
//  InstagramKit
//
//  Created by Артём Шляхтин on 07.09.16.
//  Copyright © 2016 Артём Шляхтин. All rights reserved.
//

import Foundation

extension URL {
    
    static func Authorization() -> URL {
        let path = "/oauth/authorize/?client_id=\(Instagram.ClientId)&redirect_uri=\(Instagram.RedirectURI)&response_type=code"
        let baseURL = URL(string: Instagram.BaseURL)
        let url = URL(string: path, relativeTo:baseURL)!
        return url
    }
    
    static func Media(_ token: String) -> URL {
        let path = "/v1/users/self/media/recent?access_token=\(token)"
        let baseURL = URL(string: Instagram.BaseURL)
        let url = URL(string: path, relativeTo:baseURL)!
        return url
    }

}
