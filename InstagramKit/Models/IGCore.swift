//
//  IGCore.swift
//  InstagramKit
//
//  Created by Артём Шляхтин on 07.09.16.
//  Copyright © 2016 Артём Шляхтин. All rights reserved.
//

import Foundation

private let InstagramProperties = Bundle.main.object(forInfoDictionaryKey: "InstagramKit") as? [String: String]

public struct Instagram {
    static let BaseURL = "https://api.instagram.com"
    static let AuthorizationURL = "https://api.instagram.com/oauth/authorize/"
    static let ClientId = InstagramProperties!["InstagramClientID"]! //TODO: Подумать!
    static let ClientSecret = InstagramProperties!["InstagramClientSecret"]!
    static let RedirectURI = InstagramProperties!["InstagramRedirect"]!
}

extension Instagram {
    
    static func requestAccessToken(_ code: String) -> (URL: URL, Params: String) {
        let params = "client_id=\(ClientId)&client_secret=\(ClientSecret)&grant_type=authorization_code&redirect_uri=\(RedirectURI)&code=\(code)"
        let path = "/oauth/access_token"
        let url = URL(string: BaseURL+path)!
        return (url, params)
    }
    
    public static func requestPhoto(_ count: Int, token: String) -> URL {
        let path = "/v1/users/self/media/recent?count=\(count)&access_token=\(token)"
        let url = URL(string: BaseURL+path)!
        return url
    }
    
    public static func requestOlderPhoto(_ identifier: String, count: Int, token: String) -> URL {
        let path = "/v1/users/self/media/recent?count=\(count)&access_token=\(token)&max_id=\(identifier)"
//        let path = "/v1/users/self/media/recent?access_token=\(token)&max_id=\(identifier)"
        let url = URL(string: BaseURL+path)!
        return url
    }
    
    public static func requestNewerPhoto(_ identifier: String, count: Int, token: String) -> URL {
        let path = "/v1/users/self/media/recent?count=\(count)&access_token=\(token)&min_id=\(identifier)"
        let url = URL(string: BaseURL+path)!
        return url
    }
    
}
